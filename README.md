## Instalation

1. Obtain module files.
2. Copy files to your *kohana/modules* folder.
3. Enable module in *kohana/application/bootstrap.php*.


## Customisation

1. Copy *kohana/modules/head/config/head.php* to your *kohana/application/config* folder.
2. Add to *kohana/application/config/head.php* all frequently used styles and scripts.


## Usage

1. Create **Head** object using `Head::instace();`
2. Render header content between `<head>` and `</head>` using `render()` method.


## Examples:

	echo Head::factory()->render();

	<title></title>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
  
  
  
	echo Head::factory()
		->title('Hello World!') // replace
		->title('Foo', ' - ') // add
		->title('Bar', '+') // add
		->render();

	<title>Hello World! - Foo+Bar</title>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
  
  
  
	echo Head::factory()
		->title('Hello World!')
		->css('media/main.css')
		->css(array(
			'media/screen.css' => 'screen',
			'media/print.css' => 'print',
		))
		->js('media/jquery.js')
		->favicon('favicon.ico')
		->render();

	<title>Hello World!</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link type="image/x-icon" href="/favicon.ico" rel="shortcut icon"/>
	<link type="text/css" href="/media/screen.css" rel="stylesheet" media="screen" />
	<link type="text/css" href="/media/print.css" rel="stylesheet" media="print" />
	<script type="text/javascript" src="/media/jquery.js"></script>
	

### Read [wiki](https://github.com/gander/head/wiki) on GitHub.