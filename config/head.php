<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'default' => array(
		'title' => array(),
		'meta' => array(),
		'http' => array(),
		'favicon' => array(),
		'style' => array(),
		'css' => array(),
		'script' => array(),
		'js' => array(),
	),
);
