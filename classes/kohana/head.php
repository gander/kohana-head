<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Helper functions for head-tag making
 * 
 * @package Kohana/Head
 * @category Base
 * @author Adam Gąsowski (Gander)
 * @license GPL
 * @version 2.0 beta
 * 
 * @link http://www.gander.pl/
 * @link https://github.com/gander/kohana-head/
 * 
 * @todo Method jQuery() for JavaScript/jQuery.ready(function($){...})
 */
class Kohana_Head {
    /**
     * @var string default name for instance and config
     */
    protected static $_default = "default";
    
    /**
     * @var array array of instances
     */
    protected static $_instances = array();
    
    /**
     * @var array array of base settings
     */
    protected $_base = NULL;
    /**
     * @var array array of custom tags
     */
    protected $_data = array(
        'http' => array(
            'tag' => 'meta',
            'rows' => array(),
        ),
        'meta' => array(
            'tag' => 'meta',
            'rows' => array(),
        ),
        'link' => array(
            'tag' => 'link',
            'rows' => array(),
        ),
        'css' => array(
            'tag' => 'link',
            'rows' => array(),
        ),
        'js' => array(
            'tag' => 'script',
            'rows' => array(),
        ),
        'style' => array(
            'tag' => 'style',
            'rows' => array(),
        ),
        'script' => array(
            'tag' => 'script',
            'rows' => array(),
        ),
    );
    
    /**
     * @var string page title
     */
    protected $_title = '';
    
    /**
     * @var string page description
     */
    protected $_description = '';
    
    /**
     * @var array array of page keywords
     */
    protected $_keywords = array();
    
    /**
     * Creating, loading and saving data.
     * 
     * @uses Arr::get
     * @access protected
     * 
     * @param string $type tag name
     * @param string $key unique key for data
     * @param array $params tag attributes
     * @param mixed $content tag content
     * @return mixed 
     */
    protected function _data($type, $key, $params = NULL, $content = NULL)
    {
        if (array_key_exists($type, $this->_data))
        {
            $key = md5(strtolower(trim($key)));

            if ($params === NULL AND $content === NULL)
            {
                return Arr::Get($this->_data[$type]['rows'], $key);
            }
            
            $this->_data[$type]['rows'][$key] = array(
                'params' => $params,
                'content' => $content,
            );
        }
    }
    
    /**
     * Render HTML header tags
     * 
     *     echo Head::instance()->render();
     * 
     * @uses Head::_data
     * @uses Head::meta
     * @uses HTML::attributes
     * @access public
     *
     * @return string 
     */
    public function render()
    {
        if (count($this->_data['meta']) > 1)
        {
            $this->_data['meta'] = array_reverse($this->_data['meta']);
        }

        if (count($this->_keywords))
        {
            $this->meta('keywords', implode(", ", $this->_keywords));
        }

        if (strlen($this->_description))
        {
            $this->meta('description', $this->_description);
        }

        if (count($this->_data['meta']) > 1)
        {
            $this->_data['meta'] = array_reverse($this->_data['meta']);
        }

        $content = array();
        
        $content[] = "<title>{$this->_title}</title>";
        
        foreach ($this->_data as $type=>$data)
        {
            foreach ($data['rows'] as $row)
            {
                $element = "<{$data['tag']}";

                if (is_array($row['content']) AND $cnt = count($row['content']))
                {
                    $row['content'] = implode(PHP_EOL, $row['content']);
                    
                    if ($cnt > 1)
                    {
                        $row['content'] = PHP_EOL.$row['content'].PHP_EOL;
                    }
                }

                if (is_array($row['params']))
                {
                    $element .= HTML::attributes($row['params']);
                }

                if (is_string($row['content']))
                {
                    $element .= ">{$row['content']}</{$data['tag']}>";
                }
                else
                {
                    $element .= " />";
                }

                $content[] = $element;
            }
        }

        return PHP_EOL.implode(PHP_EOL, $content).PHP_EOL;
    }
    
    /**
     * Loading config groups for head.
     * 
     *     Head::instance()->config();
     * 
     *     Head::instance()->config('example');
     * 
     * @uses Arr::get
     * @access public
     *
     * @param array $group name of config group
     * @return Head this class object
     */
    public function config($group = NULL)
    {
        if ($group === NULL)
        {
            $group = Head::$_default;
        }

        // load config
        if ($config = Arr::Get(Kohana::$config->load('head'), $group))
        {
            if (is_array($config))
            {
                foreach ($config as $method=>$data)
                {
                    if ($method AND is_string($method) AND method_exists($this, $method) AND is_array($data) AND count($data))
                    {
                        call_user_func_array(array($this, $method), $data);
                    }
                }
            }
        }

        return $this;
    }
    
    /**
     * Creating Head object.
     * 
     *     $h = Head::factory();
     * 
     * @access public
     *
     * @return Head 
     */
    public static function factory()
    {
        return new Head;
    }
    
    
    /**
     * Returns instance of Head object. If named instance not exists - create it using factory.
     * 
     *     $h = Head::instance(); // 'default'
     * 
     *     $H = Head::instance('example');
     * 
     *     echo Head::instance('example')->render();
     * 
     * @see Head::factory
     * @uses Head::factory
     * @access public
     *
     * @param string $name name of instance
     * @return Head this class object
     */
    public static function instance($name = NULL)
    {
        if ($name === NULL)
        {
            $name = Head::$_default;
        }

        if (!isset(Head::$_instances[$name]))
        {
            Head::$_instances[$name] = Head::factory();
        }

        return Head::$_instances[$name];
    }
    
    /**
     * Head object constructor.
     * 
     * @uses Head::http
     * @uses Head::config
     * @access public
     */
    public function __construct()
    {
        $this
            ->http('Content-Type', Kohana::$content_type.'; charset='.Kohana::$charset)
            ->index()
            ->protocol()
            ->config()
        ;
        
    }
    
    /**
     * Trigger rendering.
     * 
     *     echo Head::instance();
     * 
     * @see Head::render
     * @uses Head::render
     * @access public
     *
     * @return string rendered tags
     */
    public function __toString()
    {
        return $this->render();
    }
    
    /**
     * Set page title, or add part of title. If no parameter set, returns actual title.
     * 
     *     // Setting new title = 'my title'
     *     Head::instance()->title('my title');
     * 
     *     // Setting new title part = 'my title - subtitle'
     *     Head::instance()->title('subtitle', ' - ');
     * 
     *     // Returns actual title
     *     echo Head::instance()->title();
     * 
     * @access public
     *
     * @param string $title page title or title part
     * @param string $separator seperator for title part
     * @return Head this class object
     */
    public function title($title = NULL, $separator = NULL)
    {
        if ($title === NULL)
        {
            return $this->_title;
        }
        elseif (is_string($separator))
        {
            $this->_title .= $separator.$title;
        }
        else
        {
            $this->_title = $title;
        }

        return $this;
    }
    
    
    /**
     * Set page description. If not parameter set, returns actual description.
     * 
     *     // Set description
     *     Head::instance()->description('Lorem ipsum.');
     * 
     *     // Return description
     *     echo Head::instance()->description();
     * 
     * @access public
     *
     * @param string $description page description
     * @return Head this class object
     */
    public function description($description = NULL)
    {
        if ($description === NULL)
        {
            return $this->_description;
        }
        else
        {
            $this->_description = $description;
            return $this;
        }
    }
    
    
    /**
     * Set page keywords. If not parameter set, returns actual keywords as array.
     * 
     *     // Set keywords
     *     Head::instance()->keywords('lorem,ipsum');
     *     Head::instance()->keywords(array('lorem', 'ipsum'));
     * 
     *     // Return keywords
     *     print_r(Head::instance()->keywords());
     * 
     * @access public
     *
     * @param array $keys page keywords
     * @return Head this class object
     */
    public function keywords($keys = NULL)
    {
        if ($keys === NULL)
        {
            return $this->_keywords;
        }
        else
        {
            if (is_string($keys))
            {
                $keys = explode(",", $keys);
            }

            if (is_array($keys) AND count($keys))
            {
                $keys = array_map('trim', $keys);
                $keys = array_merge($this->_keywords, $keys);
                $keys = array_unique($keys);
                $keys = array_filter($keys);

                $this->_keywords = $keys;
            }
        }

        return $this;
    }
    
    
    /**
     * Add external style sheet file.
     *     
     *     Head::instance()->css('media/style.css');
     *     
     *     Head::instance()->css('http://example.com/includes/style.css');
     *     
     *     Head::instance()->css('print.css', 'print');
     *     
     * @see Head::style
     * @uses Head::_data
     * @access public
     *
     * @param string $path external style sheet file
     * @param string $media style media type
     * @return Head this class object
     */
    public function css($path, $media = NULL)
    {
        if (is_array($path))
        {
            foreach ($path as $_path=>$_media)
            {
                $this->css($_path, $_media);
            }
        }
        else
        {
            $params = array(
                'rel' => 'stylesheet',
                'href' => $this->_base($path),
                'type' => 'text/css',
            );

            if (is_string($media))
            {
                $params['media'] = $media;
            }

            $this->_data('css', $path, $params);
        }

        return $this;
    }
    
    /**
     * Add external JavaScript file.
     *     
     *     Head::instance()->js('media/script.js');
     *     
     *     Head::instance()->js('http://example.com/includes/script.js');
     *     
     * @see Head::script
     * @uses Head::_data
     * @access public
     *
     * @param string $path external javascript file
     * @return Head this class object
     */
    public function js($path)
    {
        if (is_array($path))
        {
            foreach ($path as $_path)
            {
                $this->js($_path);
            }
        }
        else
        {
            $this->_data('js', $path, array(
                'src' => $this->_base($path),
                'type' => 'text/javascript',
            ), '');
        }

        return $this;
    }
    
    /**
     * Add internal style code
     * 
     *     Head::instance()->style('body {background-color: #AAAAAA; }');
     * 
     * @see Head::css
     * @uses Head::_data
     * @access public
     *
     * @param string $code css code
     * @return Head this class object
     */
    public function style($code)
    {
        $_style = Arr::get($this->_data('style', 'code'), 'content', array());
        $_style[] = $code;

        $this->_data('style', 'code', array(
            'type' => 'text/css',
        ), $_style);

        return $this;
    }
    
    /**
     * Add internal JavaScript code
     * 
     *     Head::instance()->script('alert(document.location.href)');
     * 
     * @see Head::js
     * @uses Head::_data
     * @access public
     *
     * @param string $code javascript code
     * @return Head this class object
     */
    public function script($code)
    {
        $_script = Arr::get($this->_data('script', 'code'), 'content', array());
        $_script[] = $code;

        $this->_data('script', 'code', array(
            'type' => 'text/javascript',
        ), $_script);

        return $this;
    }
    
    /**
     * Add http-equiv meta-tag.
     * 
     *     Head::instance()->http('Content-Type', 'text/html; charset=utf-8');
     * 
     * @uses Head::_data
     * @access public
     *
     * @param string $key http-equiv key
     * @param string $value http-equiv value
     * @return Head this class object
     */
    public function http($key, $value)
    {
        if (is_array($key))
        {
            foreach ($key as $_key=>$_value)
            {
                $this->http($_key, $_value);
            }
        }
        else
        {
            $this->_data('http', $key, array(
                'http-equiv' => $key,
                'content' => $value,
            ));
        }
        
        return $this;
    }
    
    /**
     * Add meta-tag.
     * 
     *     Head::instance()->meta('X-Powered-By', 'Kohana Framework 3');
     * 
     * @uses Head::_data
     * @access public
     *
     * @param string $key meta key
     * @param string $value meta value
     * @return Head this class object
     */
    public function meta($key, $value)
    {
        if (is_array($key))
        {
            foreach ($key as $_key=>$_value)
            {
                $this->meta($_key, $_value);
            }
        }
        else
        {
            $this->_data('meta', $key, array(
                'name' => $key,
                'content' => $value,
            ));
        }
        
        return $this;
    }
    
    /**
     * Add favicon ico.
     * 
     *     Head::instance()->favicon('/favicon.png');
     * 
     * @uses Head::_data
     * @uses File::mime_by_ext
     * @access public
     *
     * @param string $file path to favicon file
     * @return Head this class object
     */
    public function favicon($file)
    {
        $params = array(
            'href' => $this->_base($file),
            'rel' => 'shortcut icon',
        );

        if ($ext = pathinfo($file, PATHINFO_EXTENSION))
        {
            $params['type'] = File::mime_by_ext($ext);
        }

        $this->_data('link', 'shortcut icon', $params);

        return $this;
    }
    
    /**
     * Refresh current page (or redirect to new) after x seconds.
     * 
     *     // Refresh current page now
     *     Head::instance()->refresh();
     * 
     *     // Refresh current page after 10 seconds
     *     Head::instance()->refresh(10);
     * 
     *     // Redirect to other page after 10 seconds
     *     Head::instance()->refresh(10, '/welcome');
     * 
     * @see Head::redirect
     * @uses Head::http
     * @access public
     *
     * @param integer $seconds timeout in seconds
     * @param string $url path to redirect
     * @return Head this class object 
     */
    public function refresh($seconds = 0, $url = NULL)
    {
        $content = (string) (int) $seconds;

        if ($url !== NULL)
        {
            $content .= "; url=".$this->_base($url);
        }

        return $this->http('Refresh', $content);
    }
    
    /**
     * Redirect to other page instantly (or after x seconds).
     * 
     *     // Redirect to other page
     *     Head::instance()->redirect('/welcome');
     * 
     *     // Redirect to other page after 10 seconds
     *     Head::instance()->redirect('/welcome', 10);
     * 
     * @see Head::redirect
     * @uses Head::refresh
     * @access public
     *
     * @param type $url path to redirect
     * @param type $seconds timeout in seconds
     * @return Head this class object 
     */
    public function redirect($url, $seconds = 0)
    {
        return $this->refresh($seconds, $url);
    }
    
    /**
     * Configure URL::Base() parameters for links.
     * 
     *     // Add default protocol to url
     *     Head::instance()->protocol(TRUE);
     * 
     *     // Add secured protocol to url
     *     Head::instance()->protocol('https');
     * 
     * @see URL::Base
     * @access public
     *
     * @param string $protocol set protocol
     * @return Head this class object 
     */
    public function protocol($protocol = NULL)
    {
        $this->_base['protocol'] = $protocol;
        return $this;
    }
    
    
    /**
     * Configure URL::Base() parameters for links.
     * 
     *     // Add "/index.php/" to url
     *     Head::instance()->index(TRUE);
     * 
     * @see URL::Base
     * @access public
     *
     * @param boolean $index add /index.php/ to url
     * @return Head this class object 
     */
    public function index($index = FALSE)
    {
        $this->_base['index'] = $index;
        return $this;
    }
    
    protected function _base($path)
    {
        if (strstr($path, "://") === FALSE)
        {
            return URL::base($this->_base['protocol'], $this->_base['index']).$path;
        }
        
        return $path;
    }
}

